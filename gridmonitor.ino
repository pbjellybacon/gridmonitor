#include <stdio.h>
#include "Adafruit_FONA.h"
#include <SoftwareSerial.h>
#include <ArduinoJson.h>
#include <EmonLib.h>

#define FONA_RX 11
#define FONA_TX 10
#define FONA_RST 9

char replybuffer[255];

SoftwareSerial fonaSS = SoftwareSerial(FONA_TX, FONA_RX);
SoftwareSerial *fonaSerial = &fonaSS;

// For FONA 800 and 808s
Adafruit_FONA fona = Adafruit_FONA(FONA_RST);
// For FONA 3G
// Adafruit_FONA_3G fona = Adafruit_FONA_3G(FONA_RST);

uint8_t readline(char *buff, uint8_t maxbuff, uint16_t timeout = 0);
uint8_t type;

EnergyMonitor emon1; // Create an instance
boolean currentState = false;
boolean previousState = false;

void setup() {
  while (!Serial);

  Serial.begin(115200);
  Serial.println(F("[GRID_MONITORING_V1]"));
  Serial.println(F("Initializing....(May take 3 seconds)"));

  setupNbIOTGateway();
  setupLineMonitor();
}

void loop() {
  double* Irms = emon1.calcIrms(1480);  // Calculate Irms only
  //Serial.println(Irms);           // Irms
  if (Irms[0] > 0.15) {
    currentState = true;
    if (currentState != previousState) {
      previousState = currentState;
      Serial.println("On");

      if (postStatus("httpbin.org/post","node1", 6.8928588,79.8656353,true,0.5,49)) {
        Serial.println("POSTED");
      } else {
        Serial.println("Error");
      }
      
    }
  }
  else {
    currentState = false;
    if (currentState != previousState) {
      previousState = currentState;
      Serial.println("Off");
       if (postStatus("httpbin.org/post","node1", 6.8928588,79.8656353,true,0.5,49)) {
        Serial.println("POSTED");
      } else {
        Serial.println("Error");
      }
    }
  }
}



// --------
void setupLineMonitor(){
  emon1.current(1, 30); // Current: input pin, calibration.
}

void setupNbIOTGateway(){
  Serial.println(F("Setting up NB-IOT gateway"));
  fonaSerial->begin(4800);
  if (! fona.begin(*fonaSerial)) {
    Serial.println(F("NB-IOT Gateway not available"));
    while (1);
  }
  type = fona.type();
  Serial.println(F("NB-IOT Gateway is OK"));
  Serial.print(F("Model: "));
  switch (type) {
    case FONA800L:
      Serial.println(F("FONA 800L")); break;
    case FONA800H:
      Serial.println(F("FONA 800H")); break;
    case FONA808_V1:
      Serial.println(F("FONA 808 (v1)")); break;
    case FONA808_V2:
      Serial.println(F("FONA 808 (v2)")); break;
    case FONA3G_A:
      Serial.println(F("FONA 3G (American)")); break;
    case FONA3G_E:
      Serial.println(F("FONA 3G (European)")); break;
    default: 
      Serial.println(F("???")); break;
  }
  
  // Print module IMEI number.
  char imei[16] = {0}; // MUST use a 16 character buffer for IMEI!
  uint8_t imeiLen = fona.getIMEI(imei);
  if (imeiLen > 0) {
    Serial.print("Module IMEI: "); Serial.println(imei);
  }

  // Configure a GPRS APN, username, and password. (Optional)
  fona.setGPRSNetworkSettings(F("dialog3g"), F(""), F(""));

  // Optionally configure HTTP gets to follow redirects over SSL. (Optional)
  //fona.setHTTPSRedirect(true);

  Serial.println("Waiting for network...");
  while (fona.getNetworkStatus() != 1);
  Serial.println("Connected to network");

  delay(5000);

  bool gprsOK = false;
  while (!gprsOK){
    if (enableGPRS()){
      gprsOK = true;
      delay(3000);
    } else {
      gprsOK = false;
    }
  }
  
}

bool postStatus(String URL, String ID, double lat, double lon, bool State, double I, double F){
  int IntAttempts = 0;
  bool IsSuccess = false;
  bool IsTimedOut = false;

  uint16_t statuscode;
  char _url[100];
  int16_t length;
  char payload[256];

  URL.toCharArray(_url, 100);

  flushFONA();

  while(!IsSuccess || !IsTimedOut){
    IntAttempts++;
    if (IntAttempts < 5) {
      // Attempt

//      if (!enableGPRS()){
//        delay (5000);
//        // flush input
//        flushSerial();
//        while (fona.available()) {
//          Serial.write(fona.read());
//        }
//        continue;
//      }

      // Gen payload
      StaticJsonBuffer<256> jsonBuffer1;
      JsonObject& root1 = jsonBuffer1.createObject();
      root1["id"] = ID;
      root1["lat"] = String(lat);
      root1["lon"] = String(lon);
      root1["state"] = State ? "true" : "false";
      root1["i"] = String(I);
      root1["f"] = String(F);
      root1.printTo(payload,sizeof(payload));
      Serial.print("JSON> ");
      Serial.println(payload);

      // POST
      if (!fona.HTTP_POST_start(_url, F("application/json"), (uint8_t *) payload, strlen(payload), &statuscode, (uint16_t *)&length)) {
        Serial.println(sprintf("POST Attempt #%d : FAILED!",IntAttempts));
        //disableGPRS();
        delay (5000);
        // flush input
        flushSerial();
        while (fona.available()) {
          Serial.write(fona.read());
        }
        continue;
      }
      
      //print response
      while (length > 0) {
        while (fona.available()) {
          char c = fona.read();
          #if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)
            loop_until_bit_is_set(UCSR0A, UDRE0); /* Wait until data register empty. */
            UDR0 = c;
          #else
            Serial.write(c);
          #endif
            length--;
          if (! length) break;
        }
      }
      Serial.println(F("\n****"));
  
      fona.HTTP_POST_end();
      //disableGPRS();

      Serial.println(sprintf("POST Attempt #%d : SUCCESS!", IntAttempts));
      IsSuccess = true;
      break;
      
    } else {
      Serial.println("POST Attempt: FAILED! (Time-out)");
      IsTimedOut = true;
      break;
    }
  }

  // flush input
  flushSerial();
  while (fona.available()) {
    Serial.write(fona.read());
  }
  return IsSuccess;
}

bool enableGPRS(){
  Serial.println(F("Enabling GPRS..."));
  if (!fona.enableGPRS(true)){
    Serial.println(F("GPRS: Disabled"));
    return false;
  } else {
    Serial.println(F("GPRS: Enabled"));
    return true;
  }
}

bool disableGPRS(){
  Serial.println(F("Disabling GPRS..."));
  if (!fona.enableGPRS(false)){
    Serial.println(F("GPRS: Enabled"));
    return false;
  } else {
    Serial.println(F("GPRS: Disabled"));
    return true;
  }
}


void flushSerial() {
  while (Serial.available())
    Serial.read();
}

void flushFONA() {
 while (fona.available() ) {
    fona.read();
  }
}

char readBlocking() {
  while (!Serial.available());
  return Serial.read();
}

uint16_t readnumber() {
  uint16_t x = 0;
  char c;
  while (! isdigit(c = readBlocking())) {
    //Serial.print(c);
  }
  Serial.print(c);
  x = c - '0';
  while (isdigit(c = readBlocking())) {
    Serial.print(c);
    x *= 10;
    x += c - '0';
  }
  return x;
}

void mergeJson(JsonObject& dest, JsonObject& src) {
   for (auto kvp : src) {
     dest[kvp.key] = kvp.value;
   }
}


