int countTrue = 1;
int countFalse = 1;
double IrmsSum;
void createSendEvent() {
  delay(1000);  // Event trigger time gap
  double* Irms = emon1.calcIrms(1480);  // Calculate Irms only
  double freq = 49.6;
  Serial.println(Irms[0]); // Irms
  if (Irms[0] > 0.36 && countTrue< 11) {
    countFalse = 0;
    countTrue++;
    if(countTrue == 10) {
      currentState = true;
      countTrue = 0;
    }
    if (currentState != previousState) {
      previousState = currentState;
      Serial.println("On");
      postState(true, Irms[0], freq);
//      if (postStatus("httpbin.org/post","node1", 6.8928588,79.8656353,true,0.5,49)) {
//        Serial.println("POSTED");
//      } else {
//        Serial.println("Error");
//      }
    }
  }
  else {
    countTrue = 0;
    countFalse++;
    if (countFalse == 10) {
      currentState = false;
      countFalse = 0;
    }
    if (currentState != previousState) {
      previousState = currentState;
      Serial.println("Off");
      postState(false, Irms[0], freq);
    }
  }
  free(Irms);
}


void postState(bool state, double _irms, double _freq){
  char payload[140];
  
  // Collect data
  
  // Battery
  Serial.println(F("Reading battery percentage left..."));
  float battLevel = ideaBoard.getBattPercent(); // Get voltage in percentage
  // Location
//  Serial.println(F("Enabling GPS.."));
//  if(!ideaBoard.enableGPS(true)){
//    Serial.println(F("Unable to Power ON GPS"));
//  }
  float lat=NULL, lon=NULL, speed_kph=NULL, heading=NULL, altitude = NULL;
  lat = 6.043600; lon = 80.208135;
//  if(!ideaBoard.getGPS(&lat, &lon, &speed_kph, &heading, &altitude)){
//    Serial.println(F("Unable to get GPS location info"));
//    
//  }
//  else{
//    Serial.println(F("Location Found.."));
////    Serial.print(F("Latitude: ")); Serial.println(lat, 6);
////    Serial.print(F("Longitude: ")); Serial.println(lon, 6);
////    Serial.print(F("Speed: ")); Serial.println(speed_kph);
////    Serial.print(F("Heading: ")); Serial.println(heading);
////    Serial.print(F("Altitude: ")); Serial.println(altitude);
////    Serial.println(F("---------------------"));
//  }

  // Gen payload
  StaticJsonBuffer<140> jsonBuffer1;
  JsonObject& root1 = jsonBuffer1.createObject();
  root1["mac"] = DEVICE_MAC_ADDRESS;
  root1["id"] = 1;
  root1["b"] = battLevel;
  root1["lat"] = lat;
  root1["lon"] = lon;
  root1["eventName"] = "DataPublish";
  root1["state"] = state ? "1" : "0";
  root1["i"] = _irms;
  root1["f"] = _freq;
  root1.printTo(payload,sizeof(payload));
 
  Serial.print(F("Publishing to topic :"));
  Serial.println(EVENT_TOPIC);
  Serial.print(F("JSON : "));
  Serial.println(payload);

  if (PubMode=="MQTT"){
    //connect to mqtt
    Watchdog.reset();
    MQTT_connect();
    Watchdog.reset();
    delay(2000);
    if (! sensor.publish(payload)) {
      Serial.println(F("Failed"));
      txfailures++;
    } else {
      Serial.println(F("OK!"));
      txfailures = 0;
    } 
  } else if (PubMode=="HTTP"){
    //post HTTP
  }
  
}


